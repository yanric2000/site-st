<?php 
    class footer{

        public $navegacao;
        public $newsletter;

        function __construct() {
            $this->navegacao = get_field('menu-navegacao', 'footer');
            $this->texto_newsletter = get_field('texto-newsletter', 'footer');
        }

        function gerar_navegacao(){

            $navegacao = $this->navegacao;
            $count = is_countable($navegacao) ? count($navegacao) : 0;
            $retorno = '
            <div class="pai-logo">
                <a href="/">
                    <img src="'. get_stylesheet_directory_uri() .'/img/logo_vermelha.svg">
                </a>
            </div>
            <div class="container-referencias">';

            for ($i=0; $i < $count; $i++) :
                
                $nome = $navegacao[$i]['nome'];
                $slug = $navegacao[$i]['slug'];
                $retorno = $retorno.
                '<div class="referencia">
                    <a href="'. $slug .'" class="fonte">'. $nome .'</a>
                </div>';
                
            endfor;

            $retorno = $retorno .'</div>';

            return $retorno;
        }
    }
?>