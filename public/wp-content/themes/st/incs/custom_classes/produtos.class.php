<?php

use function YoastSEO_Vendor\GuzzleHttp\json_encode;

class produtos{
    
    public $categorias;
    public $args_categorias;

    function __construct() {
        $this->args_categorias = array(
            'taxonomy' => 'categoria', 
            'hide_empty' => false,
            'orderby' => 'name',
            'order'   => 'ASC',
        );
        $this->args_produtos = array(
            'hide_empty' => false,
            'orderby' => 'name',
            'order'   => 'ASC',
            'post_type' => 'produtos',
        );
    }

    function todas_categorias($args = null) {
        $args = $args != null ? $this->args_categorias : '';
        return get_terms($this->args_categorias);
    }

    function dropdown_header($args = null) {
        $args = !empty($args) && $args != null ? $args : $this->args_categorias;
        $args_categorias_produto = $args;
        $categorias = get_terms($args_categorias_produto);
        $count_categorias = is_countable($categorias) ? count($categorias) : 0;
    ?>
        <div class="dropdown">
            <div class="lista-categorias">
    <?php
        
            for ($i=0; $i < $count_categorias; $i++) :
                $slug = $categorias[$i]->slug;
                $rotulo = $categorias[$i]->name;
    ?>
                <div class="categoria">
                    <a href="/produtos/#<?= $slug ?>" class="fonte rotulo"> <?= $rotulo ?> </a>
                    <div class="seta-categoria"> 
                        <img src="<?= get_stylesheet_directory_uri(); ?>/img/seta_direita.svg">
                    </div>
        <?php       
        
                $args_produtos = array(
                    'post_type' => 'produtos',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'categoria',
                            'field' => 'slug',
                            'terms' => $slug
                        )
                    )
                );
                $produtos = get_posts($args_produtos);
                $count_produtos = count($produtos);
        ?>
                    <div class="lista-produtos">
            <?php
                for ($j=0; $j < $count_produtos; $j++) : ?>
                    
                        <div class="produto"> 
                            <a href="/produtos/#<?= $slug ?> " class="fonte"> 
                                <?= $produtos[$j]->post_title ?> 
                            </a> 
                        </div>
            <?php
                endfor;
            ?>
                    </div>
                </div>
        <?php
            endfor;
        ?>
            </div>
        </div>
        <?php

        return;
    }

    function dropdown_header_responsivo($args = null) {
        $args = !empty($args) && $args != null ? $args : $this->args_categorias;
        $args_categorias_produto = $args;
        $categorias = get_terms($args_categorias_produto);
        $count_categorias = is_countable($categorias) ? count($categorias) : 0;
        
        ?>
        <div class="dropdown">
            <div class="pai-lista-categoria">
                <div class="lista-categorias">
        <?php
        
            for ($i=0; $i < $count_categorias; $i++) :
                $slug = $categorias[$i]->slug;
                $rotulo = $categorias[$i]->name;
                $args_produtos = array(
                    'post_type' => 'produtos',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'categoria',
                            'field' => 'slug',
                            'terms' => $slug
                        )
                    )
                );
                $produtos = get_posts($args_produtos);
                $count_produtos = count($produtos);

                $redirect = $count_produtos == 0 ? 'redirect="'. home_url() .'/'. $slug .'"' : '';
        ?>
                    <div class="categoria" <?= $redirect; ?>>
                        <p class="fonte rotulo"> <?=  $rotulo ?> </p>
                        <div class="seta-categoria">
                            <img src="<?= get_stylesheet_directory_uri() ?>/img/seta_direita.svg">
                        </div>
        <?php
                if($count_produtos > 0) :
        ?>
                        <div class="lista-produtos">
        <?php
                        for ($j=0; $j < $count_produtos; $j++) : ?>
                                
                            <div class="produto">
                                <a href="/produtos/#<?= $slug ?>" class="fonte"> 
                                    <?= $produtos[$j]->post_title; ?>
                                </a> 
                            </div>
        <?php
                        endfor;
        ?>
                        </div>
        <?php
                endif;
        ?>
                    </div>
        <?php
            endfor;
        ?>

                </div>
            </div> 
        </div>

        <?php
            return;
    }

    function exibir_cards($args = null){
        
        $args_padrao = array(
            'taxonomy' => 'categoria', 
            'hide_empty' => false,
            'orderby' => 'name',
            'order'   => 'ASC',
        );

        $args_categorias = $args != null ? $args : $args_padrao;

        $categorias = $this->todas_categorias($args_categorias);
        
        $count = is_countable($categorias) ? count($categorias) : 0;
        ?>

        <div class="todos-cards cards-produtos">
        
        <?php

        for ($i=0; $i < $count; $i++) :

            $nome = strlen($categorias[$i]->name) > 40 ? substr($categorias[$i]->name, 0, 40) .'...' : $categorias[$i]->name;
            $slug = $categorias[$i]->slug;
            $thumbnail = get_field('imagem-destacada', $categorias[$i])['sizes']['size_292x288'];
            $descricao = $categorias[$i]->description;

            $descricao = strlen($descricao) > 115 ? substr($descricao, 0, 115) . '..' : $descricao;
            
            ?>
                <div class="card anime-to-right ">
                    <div class="pai-imagem">
                        <a href="produtos/#<?= $slug; ?>">
                            <img src="<?= $thumbnail ?>" alt="Logo do produto">
                        </a>
                    </div>
                    <div class="texto">
                        <span><?=  $nome ?> </span>
                        <p> <?= $descricao ?> </p>
                        <a href="/produtos/#<?= $slug ?>" class="quadrado-seta">
                            <img src="<?=  get_stylesheet_directory_uri(); ?>/img/seta_direita_branca.svg" alt="Seta de redirecionamento">
                        </a>
                    </div>
                </div>
            <?php
        endfor;
        ?>

        </div>
        
        <?php
        return;
    }

    function mostrar_categorias_pagina_produtos($args = null){

        $args_categorias = $args == null ? $this->args_categorias : $args;
        $categorias = get_terms($args_categorias);
        $count = count($categorias);
    
        for ($i=0; $i < $count; $i++) :
            $slug = $categorias[$i]->slug;
            $titulo = $categorias[$i]->name;
            $descricao = $categorias[$i]->description;
            $id_categoria = $categorias[$i]->term_id;
            $args_produtos = array(
                'post_type' => 'produtos',
                'hide_empty' => false,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'categoria',
                        'field' => 'term_id',
                        'terms' => $id_categoria,
                    )
                )
            );
            $produtos = get_posts($args_produtos);
            $count_produtos = count($produtos);
        ?>
            <div class="categoria anime">
                <div class="ancora" id="<?= $slug; ?>"></div>
                <div class="container-conteudo">
                    <div class="pai-titulo">
                        <h3 class="fonte"> <?= $titulo; ?> </h3>
                    </div>
                    <div class="conteudo-centralizado">
                        <div class="pai-descricao">
                            <p> <?= $descricao; ?> </p>
                        </div>

                        
                    <?php
                    if($count_produtos > 0) :
                    ?>
                        <ul class="produtos">
                    <?php
                        for ($j=0; $j < $count_produtos; $j++) :
                            $titulo_post = $produtos[$j]->post_title;
                    ?>
                            <li class="pai-link">
                                <a href=":javascript" onclick="abrirModalContato();">
                                    <?= $titulo_post ?>
                                </a>
                            </li>
                    <?php
                        endfor;
                    endif;
                    ?>
                        </ul>
                    </div>
                    <button onclick="abrirModalContato()" class="botao-vermelho fonte-branca">contato</button>
                </div>
                <?php
                    $thumbnail = get_field('imagem-destacada', $categorias[$i])['sizes']['size_498x392']
                ?>
                <div class="container-imagem">
                    <img src="<?= $thumbnail; ?>">
                </div>
            </div>
        <?php
        endfor;

    }
}

?>