<?php
add_action('wp_enqueue_scripts', function () {

/******* FANCYBOX --- plugin para lightbox de galerias. 
Site: http://fancyapps.com/fancybox/3/ */

// wp_enqueue_style('fancybox_css', get_stylesheet_directory_uri().'/js/plugins/fancybox/jquery.fancybox.min.css');
// wp_enqueue_script('fancybox_js', get_stylesheet_directory_uri().'/js/plugins/fancybox/jquery.fancybox.min.js', array(), null, true);

/******* SLICK --- plugins para slides. 
Site: http://kenwheeler.github.io/slick/ */

wp_enqueue_style('slick_css', get_stylesheet_directory_uri().'/js/plugins/slick/slick.css');
wp_enqueue_style('slick_theme_css', get_stylesheet_directory_uri().'/js/plugins/slick/slick-theme.css');
wp_enqueue_script('slick_js', get_stylesheet_directory_uri().'/js/plugins/slick/slick.min.js', array(), null, true);

/******* JQUERY PLUGINS */
 wp_enqueue_script('validar', get_stylesheet_directory_uri().'/js/plugins/jquery.validate.min.js', array(), false, true);
 wp_enqueue_script('mascara', get_stylesheet_directory_uri().'/js/plugins/jquery.mask.min.js', array(), null, true);

 /*** Toastr ****/
 wp_enqueue_script('toastr_js', get_stylesheet_directory_uri().'/js/plugins/toastr/toastr.js', array(), null, true);
 wp_enqueue_style('toastr_css', get_stylesheet_directory_uri().'/js/plugins/toastr/toastr.css');

});