<?php
add_action('wp_enqueue_scripts', function () {    

	/******* CSS PADRÃO */
	wp_enqueue_style('css_base', get_stylesheet_directory_uri().'/src/css/base.min.css?v=1', array(), null, false);
	wp_enqueue_style('css_main', get_stylesheet_directory_uri().'/src/css/global.min.css?v=1', array(), null, false);
	wp_enqueue_style('css_vars', get_stylesheet_directory_uri().'/src/css/vars.min.css?v=1', array(), null, false);
	wp_enqueue_style('css_vars', 'https://fonts.googleapis.com/css?family=Hind+Siliguri:300,400,500,600,700,800&display=swap', array(), null, false);

	/******* JAVASCRIPT PADRÃO */
	wp_register_script('main', get_stylesheet_directory_uri().'/src/js/main.min.js?v=1', array('jquery'), null, true);
	
	wp_enqueue_script('main', array(), null, true);
});