<?php
add_action( 'init', function(){
	//Cria categoria custom para post types
	register_taxonomy(
        'slug_categoria',
        'slug_post_type',
        array(
            'label' => __( 'Nome_categoria' ),
            'rewrite' => false,
            'hierarchical' => true
        )
    );
});