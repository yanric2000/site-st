<?php
add_action( 'init', function(){
    // add_rewrite_tag('%pagina%', '([0-9\-]*)');
    // add_rewrite_tag('%item%', '([0-9a-zA-Z\-]*)');
    // add_rewrite_tag('%categoria%', '([0-9a-zA-Z\-]*)');
    // add_rewrite_tag('%filtro%', '([0-9a-zA-Z\-]*)');

    // //PAGINA FLOATER PRODUTO
    // add_rewrite_rule('^portfolio/pagina/([0-9\-]*)/item/([0-9a-zA-Z\-]*)/?','index.php?pagename=portfolio&pagina=$matches[1]&item=$matches[2]', 'top');

    // //FLOATER PRODUTO
    // add_rewrite_rule('^portfolio/item/([0-9a-zA-Z\-]*)/?','index.php?pagename=portfolio&item=$matches[1]', 'top');

    // //PAGINA CATEGORIA E FILTRO PRODUTO COM FLOATER DO ITEM
    // add_rewrite_rule('^portfolio/categoria/([0-9a-zA-Z\-]*)/filtro/([0-9a-zA-Z\-]*)/pagina/([0-9\-]*)/item/([0-9a-zA-Z\-]*)/?','index.php?pagename=portfolio&categoria=$matches[1]&filtro=$matches[2]&pagina=$matches[3]&item=$matches[4]', 'top');
    add_rewrite_tag('%pagina%', '([0-9]*)');
    add_rewrite_tag('%pesquisa%', '([0-9a-zA-Z\-])');
    add_rewrite_tag('%categoria%', '([0-9]*)');
    add_rewrite_tag('%outrapag%', '([0-9]*)');
    
    add_rewrite_rule('^blog/page/([0-9]*)/?','index.php?pagename=blog&pagina=$matches[1]', 'top');
    add_rewrite_rule('^blog/pesquisa/([0-9a-zA-Z\-]*)/?/page/([0-9]*)/?','index.php?pagename=blog&pesquisa=$matches[1]&outrapag=$matches[2]', 'top');
    add_rewrite_rule('^blog/pesquisa/([0-9a-zA-Z\-]*)/?','index.php?pagename=blog&pesquisa=$matches[1]', 'top');

    add_rewrite_rule('^blog/categoria/([0-9a-zA-Z\-+]*)/pesquisa/([0-9a-zA-Z\-]*)/page/([0-9]*)?','index.php?pagename=blog&categoria=$matches[1]&pesquisa=$matches[2]&pagina=$matches[3]', 'top');
    add_rewrite_rule('^blog/categoria/([0-9a-zA-Z\-+]*)/?/page/([0-9]*)/?','index.php?pagename=blog&categoria=$matches[1]&pagina=$matches[2]', 'top');
    add_rewrite_rule('^blog/categoria/([0-9a-zA-Z\-+]*)/?','index.php?pagename=blog&categoria=$matches[1]', 'top');

    flush_rewrite_rules();
});
