<?php
add_theme_support( 'post-thumbnails' );
add_image_size('size_380x380', 380, 380, true);

add_image_size('size_264x82', 264, 82, true);
add_image_size('size_672x638', 672, 638, true);
add_image_size('size_426x286', 426, 286, true);
add_image_size('size_292x288', 292, 288, true);
add_image_size('size_580x422', 580, 422, true);

add_image_size('size_378x282', 378, 282, true);
add_image_size('size_498x392', 488, 392, true);
add_image_size('size_1920x442', 1920, 442, true);
add_image_size('size_390x260', 390, 260, true);



add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
function wpshout_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'size_380x380' => __( 'Quadrado médio' ),
    ) );
}