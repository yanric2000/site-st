<?php
function logo_admin() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/padrao/demacode-logo.svg);
            width: auto;
            height: 65px;
            background-size: 100% 65px;
            background-repeat: no-repeat;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'logo_admin' );

function login_admin_url() {
    return 'http://demacode.com.br';
}
add_filter( 'login_headerurl', 'login_admin_url' );

function login_admin_title() {
    return 'Demacode';
}
add_filter( 'login_headertext', 'login_admin_title' );
