<?php

	wp_enqueue_style('css_home', get_stylesheet_directory_uri().'/src/css/page-home.min.css?v=1', array(), null, false);
	get_header();

	$video  = !empty(get_field('video')) ? get_field('video')['url'] : ''; // Poster Image Field Name

	if($video != '') :

?>

<div class="main-video">
	<div class="container-video carregando">
		<video autoplay="true" loop muted>
			<source src="<?= $video; ?>">
		</video>
		<div class="texto">
			<?= get_field('titulo-video-superior'); ?>
		</div>
		<div class="container-seta">
			<a href="#secao-apos-video">
				<img src="<?= get_template_directory_uri() ?>/img/arrow-down.svg">
			</a>
		</div>
	</div>
</div>

<?php

	endif;

?>

<div class="secao-apos-video" id="secao-apos-video">
<?php

	$secao = get_field('secao-apos-video');
?>
	<div class="container-padrao">
		<div class="filho-container">
			<div class="container-imagem anime-to-right">
				<img  src="<?= $secao['imagem']['sizes']['size_580x422']; ?>">
				<img class="selo" src="<?= get_stylesheet_directory_uri() ?>/img/logo-flutuante.svg" alt="Selo sobre a imagem">
			</div>
			<div class="descricao">
				<h1 class="fonte"> <?= $secao['titulo']; ?> </h1>
				<?= $secao['texto-corrido']; ?>
				<a onclick="abrirModalContato()" class="botao-vermelho fonte-amarela"> contato </a>
			</div>
		</div>
	</div>

	<div class="vantagens">
		<div class="linha-vermelha"></div>
		<div class="container-padrao">
			<div class="filho-container">
				<div class="legenda anime">
					<img src="<?= get_stylesheet_directory_uri() ?>/img/caminhao.svg" alt="Ícone caminhão">
					<div class="container-texto">
						<?= $secao['texto-icone-caminhao']; ?>
					</div>
				</div>
				<div class="legenda anime">
					<img src="<?= get_stylesheet_directory_uri() ?>/img/navio.svg" alt="Ícone navios">
					<div class="container-texto">
						<?= $secao['texto-icone-navio']; ?>
					</div>
				</div>
				<div class="legenda anime">
					<img src="<?= get_stylesheet_directory_uri() ?>/img/compras.svg" alt="Ícone compra">
					<div class="container-texto">
						<?= $secao['texto-icone-compras']; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="tudo-produtos">
	<div class="container-padrao">
		<div class="filho-container">
			<h3 class="fonte">Fios a pronta entrega</h3>
				<?php
					$produto = new produtos();
					echo ($produto->exibir_cards());
				?>
		</div>
	</div>
</div>

<div class="importacao">
	<div class="retangulo esquerda"></div>
	<div class="container-padrao ajustes">
		<div class="filho-container">
			<h3 class="fonte"> IMPORTAÇÃO <span class="fonte">de fios</span> </h3>
			<div class="descricao">
				<img src="<?= get_field('imagem-importacao')['sizes']['size_390x260']; ?>" alt="Imagem da seção">
				<div class="container-texto">
					<?= get_field('texto-importacao'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="retangulo direita"></div>
</div>

<div class="contato">
	<div class="container-padrao">
		<div class="filho-container">
			<h2 class="fonte">
				<?= get_field('titulo-contato'); ?>
			</h2>
			<div class="cards">
				<a href="#ancora-map">
					<div class="card anime">
						<div class="pai-imagem">
							<img src="<?= get_stylesheet_directory_uri() ?>/img/marker-cinza.svg" alt="Ícone marcador de mapa">
						</div>
						<strong>Endereço</strong>
						<p> <?= get_field('endereco-completo', 'geral'); ?> </p>
					</div>
				</a>
				<div class="card email anime">
					<div class="pai-imagem">
						<img src="<?= get_stylesheet_directory_uri() ?>/img/email-cinza.svg" alt="Ícone e-mail">
					</div>
					<strong>E-mail</strong>
					<p> <?= get_field('email', 'geral'); ?> </p>
				</div>
				<div class="card anime" onclick="copyTextToClipboard('<?= get_field('telefone', 'geral'); ?>')">
					<div class="pai-imagem">
						<img src="<?= get_stylesheet_directory_uri() ?>/img/telefone-cinza.svg" alt="Ícone telefone">
					</div>
					<strong>Telefone</strong>
			<?php
				$telefones = get_field('telefones');
				$count = is_countable($telefones) ? count($telefones) : 0;

				for ($i=0; $i < $count; $i++) :
					$telefone = $telefones[$i]['numero'];
			?>
					<p> <?= $telefone; ?> </p>
			<?php		
				endfor;
			?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-mapa anime">
	<div id="ancora-map"></div>
	<div id="map"></div>
</div>

<script>

	jQuery(document).ready(function($){
		
		

	});

</script>

<script>
	
	var map;
	
	function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -34.397, lng: 150.644},
			zoom: 8
		});
	}
	
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuTWKmT4iHOYE-vYZSCAENf77-JkGLB5M&callback=initMap"
	async defer></script>

<?php get_footer();?>
