<?php
    
    wp_enqueue_style('css_single_blog', get_stylesheet_directory_uri().'/src/css/single-blog.min.css?v=1', array(), null, false);
    get_header();
    $pagina_blog = get_page_by_path('blog');
?>

<div class="banner centralizar camada-degrade" style="background-image: url(<?= get_field('banner-pagina', $post)['sizes']['size_1920x404']; ?>)">
    <div class="conteudo-sobreposto centralizar">
        <div class="etiqueta centralizar fonte">NEWS</div>
        <div class="header-texto">
            <?= get_field('texto-banner', $pagina_blog); ?>
        </div>
        <a href="<?= get_field('link-publicacao', $pagina_blog); ?>" class="fonte centralizar"> VER PUBLICAÇÃO </a>
    </div>
</div>

<div class="breadcrumb do-blog">
    <div class="container-padrao">
        <div class="filho-container">
            <a href="/"> Início -&nbsp;	</a>
            <a href="/blog"> Blog -&nbsp;	</a>
            <p> <?= the_title(); ?> </p>
        </div>
    </div>
</div>

<div class="post">
    <div class="container-padrao">
        <div class="filho-container">
            <div class="container-conteudo">
                <div class="introducao texto-corrido fonte">
                    <?= get_field('introducao', $post); ?>
                </div>
            <?php
                $carrossel = get_field('carrossel-imagens', $post);
                $count = is_countable($carrossel) ? count($carrossel) : 0;
                if($count > 0) :
            ?>
                <div class="container-carrossel anime">
                    <div class="carrossel">
                    <?php
                        for ($i=0; $i < $count; $i++) :
                    ?>
                        <div class="slide">
                            <img src="<?= $carrossel[$i]['imagem']['sizes']['size_780x468']; ?>">
                        </div>
                    <?php endfor; ?>
                    </div>
                </div>
            <?php endif; ?>
                <div class="texto-corrido fonte">
                    <?= $post->post_content; ?>
                </div>
                <div class="compartilhar">
                    <div class="container-titulo padrao">
                        <div class="risco-cinza"></div>
                        <img src="<?= get_stylesheet_directory_uri() ?>/img/share.svg" >
                        <h1 class="fonte">compartilhar</h1>
                        <div class="risco-cinza"></div>
                    </div>
                    <div class="comentarios">
                    <div class="fb-comments" data-width="800" data-href="<?= get_home_url() . '/blog/'. $post->post_name; ?>" data-numposts="5"></div>
                    <div id="fb-root"></div></div>
                </div>
                <div class="paginacao">
                <?php
                    echo previous_post_link('%link', '< ler notícia anterior');
                    echo next_post_link('%link', 'ler a próxima notícia >');
                ?>
                </div>
            </div>

            <div class="barra-lateral">
                <div class="fundo-preto" onclick="togleMenuLateral()"></div>
                <div class="togle-menu-lateral centralizar" onclick="togleMenuLateral()">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/double-arrow-left.svg">
                </div>
                <div class="container-para-scroll">
                    <div class="icone-fechar">
                        <img src="<?= get_stylesheet_directory_uri() ?>/img/cancel.svg" onclick="togleMenuLateral()">
                    </div>
                    <div class="artigos-destaque">
                        <h2 class="fonte">Artigos mais vistos</h2>
                    <?php
                        $args = array(
                            'posts_per_page'      => 3,                 // Máximo de 5 artigos
                            'no_found_rows'       => true,              // Não conta linhas
                            'post_status'         => 'publish',         // Somente posts publicados
                            'ignore_sticky_posts' => true,              // Ignora posts fixos
                            'orderby'             => 'meta_value_num',  // Ordena pelo valor da post meta
                            'meta_key'            => 'tp_post_counter', // A nossa post meta
                            'order'               => 'DESC'             // Ordem decrescente
                        );
                        
                        $posts_destaque = get_posts($args);
                        $count = is_countable($posts_destaque) ? count($posts_destaque) : 0;

                        for ($i=0; $i < $count; $i++) :
                            $meu_post = $posts_destaque[$i];
                            $nome = $meu_post->post_name;
                            $titulo = $meu_post->post_title;
                            $content = $meu_post->post_content;
                            $slug = $meu_post->slug;
                    ?>
                        <div class="artigo">

                            <div class="post-destaque">
                                <div class="header-imagem">
                                    <a href="/blog/<?= $nome; ?>">
                                    <div class="pai-imagem">
                                        <img src="<?= get_the_post_thumbnail_url($meu_post, 'size_278x164'); ?>"> 
                                    </div>
                                    </a>
                                </div>

                                <div class="categorias-post">
                                    <?php
                                        $caracter_separador = ", ";
                                        echo links_das_etiquetas($meu_post, $caracter_separador); 
                                    ?> 
                                </div>
                                <a class="fonte-titulo" href="/blog/<?= $nome; ?>">
                                    <?= $titulo; ?>
                                </a>
                            </div>
                        </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function togleMenuLateral(){
        let barraLateral = document.querySelectorAll('.post .filho-container .barra-lateral')[0];
        let classe = "active";
        
        controleClasse(barraLateral, classe);
    }

    jQuery(document).ready(function($){

        $('.carrossel').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            arrows: true,
			dots: false,
			nextArrow: '<button type="button" class="seta-direita next-arrow"> <img src="<?= get_template_directory_uri() ?>/img/seta-direita.svg"> </button>',
            prevArrow: '<button type="button" class="seta-esquerda prev-arrow"> <img src="<?= get_template_directory_uri() ?>/img/seta-esquerda.svg"> </button>',
            responsive: [
                {
                breakpoint: 640,
                settings: {
                    arrows: false,
                    dots: true,
                }
                },
            ]
        });

        $(window).resize(function(){
            
            var widthTela = $(window).width();
            var barraLateral = $('.post .filho-container .barra-lateral')[0];

            if(widthTela > 1280 && barraLateral.classList.contains('active')){
                togleMenuLateral();
            }
        });

    });

</script>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v4.0&appId=2036849013080627&autoLogAppEvents=1"></script>
<?php
    get_footer();
?>