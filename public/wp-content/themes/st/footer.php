	</section>
	<footer class="main-footer">
		<div class="container-padrao">
			<div class="filho-container">
				<div class="navegacao">
					<?php
						$footer = new footer;
						echo $footer->gerar_navegacao();
					?>
				</div>
				<div class="newsletter">
					<div class="legenda">
						<img src="<?= get_template_directory_uri() ?>/img/email.svg">
						<div class="container-texto">
							<?= get_field('legenda-newsletter', 'footer'); ?>
						</div>
					</div>
					<form class="formulario" method="POST" action="<?= admin_url('admin-ajax.php') ?>" data-hibrido-subscribers-form>
						<input class="fonte" type="email" required name="email" placeholder="Digite seu E-mail">
						<button type="submit" class="botao-vermelho fonte-branca">enviar</button>
						<div class="callback" data-hibrido-subscribers-response>
						</div>
					</form>
				</div>
			</div>
		</div>
	</footer>

	<div class="modal-contato">
		<h2 class="fonte">Fale Conosco</h2>
		<form action="<?= admin_url('admin-ajax.php'); ?>" method="POST" class="formulario" data-hc-form>
			<div class="row fonte">
				<label for="nome-modal-contato" hc-mail-from>Nome</label>
				<input type="text" id="nome-modal-contato" placeholder="Nome e sobrenome" required>
			</div>
			<div class="row fonte">
				<label for="email-modal-contato">E-mail</label>
				<input hc-mail-from type="email" id="email-modal-contato" placeholder="E-mail" required>
			</div>
			<textarea name="mensagem" cols="30" rows="10" class="fonte" placeholder="Escreva a sua mensagem aqui" hc-mail-message></textarea>
			<button type="submit" class="botao-amarelo-claro fonte-vermelha" required>enviar</button>
			<p data-hc-feedback></p>
		</form>
	</div>

	<div class="fundo-escuro"></div>

	<div id="callback-copiado">Texto copiado para área de transferencia</div>

	<div class="risco-vermelho"></div>

	<div class="seta-voltar-topo centralizar">
		<img src="<?= get_stylesheet_directory_uri() ?>/img/arrow_up.svg" alt="Seta pra cima">
	</div>

<script>
	
	jQuery(document).ready(function($){
		$('.navegacao .container-referencias .referencia a[href=contato]').on('click', function(e){
			e.preventDefault();
			abrirModalContato();
			return false;
		});
	});

	function abrirDropdown(el){

		var seta = el;
		var pai = seta.parentElement.parentElement;
		var numeroFilhos = pai.childElementCount - 1;
		var tamanhoMedioTopico = 40;
		var novaAltura = tamanhoMedioTopico * numeroFilhos;
		var classe = "active";

		if(pai.classList.contains('active')){
			pai.style.height = '20px';
			removerClasse(pai, classe);
			removerClasse(seta, classe);
		}else{
			pai.style.height = novaAltura + 'px';
			adicionarClasse(pai, classe);
			adicionarClasse(seta, classe);
		}
	}

	jQuery(document).ready(function($){
		$('.secao-conteudo').on('click', fecharMenuHeader());
	});

	function fallbackCopyTextToClipboard(text) {
		let textArea = document.createElement("textarea");
		textArea.style.position = 'fixed';
		textArea.style.width = '0px';
		textArea.value = text;
		document.body.appendChild(textArea);
		textArea.focus();
		textArea.select();
	
		try {
			let successful = document.execCommand('copy');
			let msg = successful ? 'successful' : 'unsuccessful';
			if(msg == 'successful'){
				callbackCopiadoSucesso();
			}
		} catch (err) {
			console.error('Fallback: Oops, unable to copy'+ err);
		}
	
		document.body.removeChild(textArea);
	}
	
	function copyTextToClipboard(text) {
		if (!navigator.clipboard) {
			fallbackCopyTextToClipboard(text);
		return;
		}
		navigator.clipboard.writeText(text).then(function() {
			callbackCopiadoSucesso();
		}, function(err) {
			console.error('Async: Could not copy text: ', err);
		});
	}

	function callbackCopiadoSucesso(){
		
		var callback = document.getElementById('callback-copiado');
		var classeSucesso = 'active';

		adicionarClasse(callback, classeSucesso);
		setTimeout(() => {
			removerClasse(callback, classeSucesso);
		}, 3000);

	}

	function fecharMenuHeader(){

		var itensAtivos = $('.header .filho-container .menu-desktop .active');
		var classe = 'active';

		resetItens(itensAtivos, classe);
	}

	function fecharMenuHeaderResponsivo(){

		var itensAtivos = $('.header .filho-container .menu-desktop .active');
		var classe = 'active';

		resetItens(itensAtivos, classe);
	}

	function resetItens(itens, classe){

		if(itens == null || itens == undefined){
			return false;
		}
		var countItensReset = itens.length;

		for (var i = 0; i < countItensReset; i++) {
			removerClasse(itens[i], classe);
		}
	}

	function abrirProdutos(containerProdutos, classe){
		let alturaProduto = 38;
		let margemRespiro = 44;
		let classeAlturaZerada = 'altura-zerada';

		controleClasse(containerProdutos, classe);
		containerProdutos.style.height = (containerProdutos.childElementCount * alturaProduto) + margemRespiro +'px';
		removerClasse(containerProdutos, classeAlturaZerada);
	}

	function destacarCategoria(containerDestaque, classe){

		let containersCategorias = $('.header .filho-container .dropdown .lista-categorias .categoria');
		let rotulo = containersCategorias[0].querySelector('.rotulo');

		resetItens(containersCategorias, classe);
		adicionarClasse(containerDestaque, classe);
		adicionarClasse(rotulo, classe);
	}

	function abrirDropdownProdutosHeaderResponsivo(){

		let containerProdutos = document.querySelector('.header .filho-container .menu-responsivo li a.link-produtos');
		let dropdownHeader = document.querySelector('.header .filho-container .menu-responsivo .dropdown');
		let todasCategoriasDropdown = document.querySelectorAll('.header .filho-container .menu-responsivo .dropdown .lista-categorias .categoria');
		let classeAtivar = 'active';
		let alturaFilhos = 48;
		let paddingTotalTopBottom = 48;

		dropdownHeader.style.height = (todasCategoriasDropdown.length * alturaFilhos) + paddingTotalTopBottom +'px';
		adicionarClasse(containerProdutos, classeAtivar);
		adicionarClasse(dropdownHeader, classeAtivar);

	}

	function fecharDropdownProdutosHeaderResponsivo(){

		let itensAtivos = document.querySelectorAll('.header .filho-container .menu-responsivo .pai-link-produtos .active');
		let classeAtivar = 'active';
		let dropdown = document.querySelector('.header .filho-container .menu-responsivo .dropdown');

		dropdown.style.height = 0 +'px';
		resetItens(itensAtivos, classeAtivar);
	}


	jQuery(function($){
		// Responsivo Header

		let containerProdutosResponsivo = $('.header .filho-container a.link-produtos');
		let categoriasDropdownResponsivo = $('.header .filho-container .dropdown .lista-categorias .categoria');
		let containerProdutosHeaderResponsivo = $('.header .filho-container .dropdown .lista-categorias');	
		let linksAnulado = $('.header .filho-container .dropdown .lista-categorias .categoria a')

		containerProdutosResponsivo.on('click', function(e){
			e.preventDefault();

			console.log('toggle dropdown');
			if(this.classList.contains('active')){
				fecharDropdownProdutosHeaderResponsivo();
				return true;
			}

			abrirDropdownProdutosHeaderResponsivo();
			return true;
		});

		categoriasDropdownResponsivo.click(function(e){

			e.preventDefault();

			let produtosAtivos = $('.header .filho-container .menu-responsivo .dropdown .lista-categorias .categoria .lista-produtos.active .active')
			let containerProdutosDropdown = this.querySelector('.lista-produtos');
			let itensParaReset = $('.header .filho-container .dropdown .lista-categorias .categoria .active');
			let classe = 'active';
			let classeAlturaZerada = 'altura-zerada';
			let redirectPorNaoTerProdutosFilhos = this.getAttribute('redirect');

			resetItens(itensParaReset, classe);

			if(redirectPorNaoTerProdutosFilhos != null){
				window.location.href = redirectPorNaoTerProdutosFilhos;
			}
			if(this.classList.contains('active')){
				console.log(this);
				removerClasse(this, classe);
				adicionarClasse(containerProdutosDropdown, classeAlturaZerada);
			}else{
				console.log(this);
				destacarCategoria(this, classe);
				abrirProdutos(containerProdutosDropdown, classe);
			}

		});

		containerProdutosHeaderResponsivo.mouseleave(function(){

			let todosOsItensAtivos = this.querySelectorAll('active');
			let classe = 'active';

			resetItens(todosOsItensAtivos, classe);
		});

		linksAnulado.on('click', function(event){
			
			event.preventDefault();
			window.location.href = this.href;

			return true;
		});
	});


	</script>
    <?php wp_footer();?>
</body>
</html>