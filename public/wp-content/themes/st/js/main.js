jQuery(document).ready(function($){
	var widthTela = $(window).width();
	
	$target = $('.anime'),
      animationClass = 'anime-init',
      windowHeight = $(window).height(),
	  offset = windowHeight - (windowHeight / 4);
	var root = document.documentElement;
	root.className += ' js';

	function boxTop(idBox) {
		var boxOffset = $(idBox).offset().top;
		return boxOffset;
	}

	function animeScroll() {
		var documentTop = $(document).scrollTop();
		$target.each(function() {
		if (documentTop > boxTop(this) - offset) {
			$(this).addClass(animationClass);
		}
		});
	}
	
	animeScroll();

	$(document).scroll(function() {
		$target = $('.anime');
		animeScroll();
	});



	$target2 = $('.anime-to-right'),
      animationClass2 = 'anime-init',
      windowHeight2 = $(window).height(),
	  offset2 = windowHeight2 - (windowHeight2 / 4);
	var root2 = document.documentElement;
	root.className += ' js';

	function boxTop(idBox) {
		var boxOffset = $(idBox).offset().top;
		return boxOffset;
	}

	function animeScroll2() {
		var documentTop = $(document).scrollTop();
		$target2.each(function() {
		if (documentTop > boxTop(this) - offset2) {
			$(this).addClass(animationClass2);
		}
		});
	}
	
	animeScroll2();

	$(document).scroll(function() {
		$target2 = $('.anime-to-right');
		animeScroll2();
	});

	var behavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	options = {
		onKeyPress: function (val, e, field, options) {
			field.mask(behavior.apply({}, arguments), options);
		}
	};
	
	$('.telefone').mask(behavior, options);

	function abrirDropdownProdutosHeader(){

		var containerProdutos = document.querySelector('.header .filho-container li a.link-produtos');
		var dropdownHeader = document.querySelector('.header .filho-container .menu-desktop .dropdown');
		var todasCategoriasDropdown = document.querySelectorAll('.header .filho-container .dropdown .lista-categorias .categoria');
		var classeAtivar = 'active';
		var alturaFilhos = 48;
		var paddingTotalTopBottom = 48;
		
		dropdownHeader.style.height = (todasCategoriasDropdown.length * alturaFilhos) + paddingTotalTopBottom +'px';
		adicionarClasse(containerProdutos, classeAtivar);
		adicionarClasse(dropdownHeader, classeAtivar);

		console.log('345');
	}

	function fecharDropdownProdutosHeader(){

		var itensAtivos = document.querySelectorAll('.header .filho-container .menu-desktop .pai-link-produtos .active');
		var classeAtivar = 'active';
		var dropdown = document.querySelector('.header .filho-container .menu-desktop .dropdown');
		
		dropdown.style.height = 0 +'px';
		resetItens(itensAtivos, classeAtivar);
	}

	var containerProdutos = $('.header .filho-container a.link-produtos');
	var categoriasDropdown = $('.header .filho-container .menu-desktop .dropdown .lista-categorias .categoria');
	var containerProdutosHeader = $('.header .filho-container .dropdown .lista-categorias');

	containerProdutos.on('click', function(e){
		e.preventDefault();
		if(this.classList.contains('active')){
			fecharDropdownProdutosHeader();
			return true;
		}
		abrirDropdownProdutosHeader();
		return true;
	});
	
	categoriasDropdown.hover(function(){

		var produtosAtivos = $('.header .filho-container .dropdown .lista-categorias .categoria .lista-produtos.active .active')
		var containerProdutosDropdown = this.querySelector('.lista-produtos');
		var rotulosDasCategorias = $('.header .filho-container .dropdown .lista-categorias .rotulo');
		var rotulo = this.querySelector('.rotulo');
		var classe = 'active';

		console.log('123');

		resetItens(produtosAtivos, classe);
		resetItens(rotulosDasCategorias, classe);

		controleClasse(containerProdutosDropdown, classe);
		controleClasse(rotulo, classe);
	});

	categoriasDropdown.mouseleave(function(){
		
		var listaCategorias = $('.header .filho-container .dropdown .lista-categorias');
		var itensAtivosDropdown = $('.header .filho-container .dropdown .active');
		var classe = 'active';

		resetItens(listaCategorias, classe);
		resetItens(itensAtivosDropdown, classe);
	});

	containerProdutosHeader.mouseleave(function(){

		var todosOsItensAtivos = this.querySelectorAll('active');
		var classe = 'active';

		resetItens(todosOsItensAtivos, classe);
	});
	
	$('body .secao-conteudo').on('click', fecharMenuHeader());

	$('body .secao-conteudo').on('click', fecharMenuHeaderResponsivo());
	
	// tava aq
	

	$('.header .filho-container .toggle-menu').on('click', function(){
		
		var menuResponsivo = $('.header .filho-container .menu-responsivo')[0];
		var toggleMenuResponsivo = $('.header .filho-container .toggle-menu')[0];
		var containerMenuResponsivo = $('.header .filho-container .container-menu-responsivo')[0];
		var classe = 'active';
		var conteudoPagina = $('.secao-conteudo')[0];
		var classeIndexMenor = 'index-negativo';

		if(menuResponsivo.classList.contains(classe)){
			toggleMenuResponsivo.classList.remove(classe);
			menuResponsivo.classList.remove(classe);
			containerMenuResponsivo.classList.remove(classe);
			removerClasse(conteudoPagina, classeIndexMenor);
		}else{
			toggleMenuResponsivo.classList.add(classe);
			menuResponsivo.classList.add(classe);
			containerMenuResponsivo.classList.add(classe);
			adicionarClasse(conteudoPagina, classeIndexMenor);
		}
	});

	var todasListasProdutos = $('.header .filho-container .container-menu-responsivo .menu-responsivo .lista-categorias .lista-produtos');
	var countListaProdutos = todasListasProdutos.length;

	for (var i = 0; i < countListaProdutos; i++) {
		var lista = todasListasProdutos[i];
		lista.classList.add('altura-zerada');
	}

	$('.fundo-escuro').on('click', function(){
		
		var modalContato = $('.modal-contato')[0];
		var fundoEscuro = $('.fundo-escuro')[0];
		var classeAbrir = 'active';

		removerClasse(modalContato, classeAbrir);
		removerClasse(fundoEscuro, classeAbrir);
	});

	$('.contato .filho-container .cards .card.email').on('click', function(){
		abrirModalContato();
	});

	$('.categoria .pai-link>a').on('click', function(){
		
		var modalContato = $('.modal-contato')[0];
		var fundoEscuro = $('.fundo-escuro')[0];
		var classeAbrir = 'active';

		adicionarClasse(modalContato, classeAbrir);
		adicionarClasse(fundoEscuro, classeAbrir);
		this.click();
	});

	$(function(){
		$(window).scroll(function() {
			var $myDiv = $('#minha-linha');
			var st = $(this).scrollTop();
			$myDiv.height( st );
			if( st == 0 ) {
				$myDiv.hide();
			} else {
				$myDiv.show();
			}
		}).scroll();
	})​;

	$('.seta-voltar-topo').on('click', function(){
		document.body.scrollTop = 0;
  		document.documentElement.scrollTop = 0;
	});

	$('.header .filho-container .dropdown .categoria > p').on('click', function(e){
		e.preventDefault();
		return false;
	});

});

jQuery(window).resize(function($){

	jQuery(function($){
		var widthTela = $(window).width();
		var toggleMenuResponsivo = $('.header .filho-container .toggle-menu')[0];
		var classeAtiva = 'active';

		if(widthTela > 860){
			var itensResetHeader = $('.header .filho-container .container-menu-responsivo .active');
			var containerResponsivo = $('.header .filho-container .container-menu-responsivo');

			removerClasse(toggleMenuResponsivo, classeAtiva);
			resetItens(itensResetHeader, classeAtiva);
			resetItens(containerResponsivo, classeAtiva);
		}
	});
});

function isScrolledIntoView(elem){
	var docViewTop = jQuery(window).scrollTop();
	var docViewBottom = docViewTop + jQuery(window).height();

	var elemTop = jQuery(elem).offset().top;
	var elemBottom = elemTop + jQuery(elem).height();

	var variavel = docViewBottom - docViewTop;
	variavel = variavel - (variavel * 0.8);
	return (elemTop + variavel <= docViewBottom );
}

function controleClasse(el, classe){
	
	if(el.tagName == undefined || el.tagName == null){
		return false;
	}
	
	if(classe == undefined || classe == null){
		return false;
	}

	if(el.classList.contains(classe)){
		el.classList.remove(classe);
	}else{
		el.classList.add(classe);
	}

	return true;
}

function controleClasseSeletor(sel, classe){
	
	var el = document.getElementById(sel);

	if(sel == undefined || sel == null || sel == "" || 
	   el == undefined || el == null
	){
		return false;
	}

	if(el.classList.contains(classe)){
		el.classList.remove(classe);
	}else{
		el.classList.add(classe);
	}
}

function removerClasse(el, classe){
	if(el == undefined || el == null){
		return false;
	}		
	el.classList.remove(classe);
	return true;
}

function adicionarClasse(el, classe){
	if(el == undefined || el == null){
		return false;
	}
	if(!el.classList.contains(classe)){
		el.classList.add(classe);
	};
	return true;
}
