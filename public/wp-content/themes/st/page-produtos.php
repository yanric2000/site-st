<?php
    get_header();
    wp_enqueue_style('css_produtos', get_stylesheet_directory_uri().'/src/css/page-produtos.min.css?v=1', array(), null, false);
    get_the_post_thumbnail_url($post, 'size_1920x404');
?>


<div class="banner-superior" style="background-image: url(<?= get_field('banner_superior')['sizes']['size_1920x442']; ?>)">
    <div class="container-padrao">
        <div class="filho-container">
            <div class="frase fonte">
                <?= get_field('texto-banner'); ?>
            </div>
        </div>
    </div>
</div>


<div class="engloba-produtos">
    <div class="container-padrao">
        <div class="filho-container">
            <div class="linha-transversal" id="minha-linha"></div>
        <?php
            
            $args = null;
            $produtos = new produtos();
            $produtos->mostrar_categorias_pagina_produtos();
            
        ?>
        </div>
    </div>
</div>

<script>


</script>

<?php get_footer(); ?>