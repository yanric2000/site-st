<?php
//******* LINKS PARA SCRIPTS CSS E JS */

/******* Scripts padrões *******/
include __DIR__."/incs/links_base.php"; 

/******* Links de plugins *******/
include __DIR__."/incs/links_plugins.php"; 


/******************************/


//****** CUSTOM */

/******* Custom config *******/
include __DIR__."/incs/custom_config.php";

/******* Custom image size *******/
include __DIR__."/incs/custom_image_sizes.php";

/******* Custom URL *******/
include __DIR__."/incs/custom_url.php";

/******* Custom post type *******/
include __DIR__."/incs/custom_post_type.php";

/******* Custom taxonomy *******/
include __DIR__."/incs/custom_taxonomy.php";

//****** INFORMAÇÕES GLOBAIS

//include __DIR__."/incs/custom_info.php";


/******************************/


//******* ACF OPTIONS */

/******* Configurações para o acf options ********/
include __DIR__."/incs/acf_options.php"; 

// Classes personalizadas

require_once(__DIR__."/incs/custom_classes/produtos.class.php");
require_once( __DIR__."/incs/custom_classes/footer.class.php");


$lista_meses =  json_encode(array(
    1 => 'Jan.',
    2 => 'Fev.',
    3 => 'Mar.',
    4 => 'Abr.',
    5 => 'Maio',
    6 => 'Jun.',
    7 => 'Jul.',
    8 => 'Ago.',
    9 => 'Set.',
    10 => 'Out.',
    11 => 'Nov.',
    12 => 'Dez.',
));

define('JSON_LISTA_MESES_ABREVIADO', $lista_meses);

// Verifica se não existe nenhuma função com o nome tutsup_session_start
if ( ! function_exists( 'tutsup_session_start' ) ) {
    // Cria a função
    function tutsup_session_start() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }
    // Executa a ação
    add_action( 'init', 'tutsup_session_start' );
}

function clear_html($string){
    // Strip HTML Tags
    $clear = strip_tags($string);
    // Clean up things like &amp;
    $clear = html_entity_decode($string);
    // Strip out any url-encoded stuff
    $clear = urldecode($string);
    // Replace non-AlNum characters with space
    $clear = preg_replace('/[^A-Za-z0-9]/', ' ', $string);
    // Replace Multiple spaces with single space
    $clear = preg_replace('/ +/', ' ', $string);
    // Trim the string of leading/trailing space
    $clear = trim($string);

    return $clear;
}

function modify_jquery_version() {
    if (!is_admin() == true) {
        wp_deregister_script( 'jquery' );
        // Change the URL if you want to load a local copy of jQuery from your own server.
        wp_register_script( 
            'jquery', 
            'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
            array(), 
            '3.1.1' );
    }

   
 }
 add_action('template_redirect', 'modify_jquery_version');