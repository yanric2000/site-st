<?php  do_action('before_wphead_after_init');?>
<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">
<head>
	<?php 
	// Tirar o comentário se for utilizar o contact form
	 HC::init();
	?>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0" charset="utf-8" http-equiv="Content-Type" />
	<title><?php echo wp_title();?></title>
    <!-- FONT AWESOME - ICONES PERSONALIZADOS -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css?v=1" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<!-- FONTE DO SITE -->
	<link href="https://fonts.googleapis.com/css?family=Barlow:400,500,600,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/header.min.css?v=1">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/footer.min.css?v=1">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css" integrity="sha256-zlmAH+Y2JhZ5QfYMC6ZcoVeYkeo0VEPoUnKeBd83Ldc=" crossorigin="anonymous" /> -->
	<?php wp_head();?>
</head>

<body>
<div class="index-header">
    <header class="header">
		<div class="container-padrao">
			<div class="filho-container">
				<ul class="menu-desktop">
					<li>
						<a class="fonte" href="/">Home</a>
					</li>
					<li class="pai-link-produtos">
						<a class="fonte link-produtos" href="/produtos">
							Produtos
							<img src="<?= get_stylesheet_directory_uri() ?>/img/arrow_down.svg" class="seta-dropdown">
						</a>
						<?php
							$produtos = new produtos();
							echo $produtos->dropdown_header();
						?>
					</li>
					<div class="header-logo">
						<a href="/">
							<img src="<?= get_stylesheet_directory_uri() ?>/img/logo.svg">
						</a>
					</div>
					<li>
						<a class="fonte" href="/#secao-apos-video">Sobre Nós</a>
					</li>
					<li>
						<a class="fonte" onclick="abrirModalContato()">Contato</a>
					</li>
				</ul>

				<div class="whatsapp">
					<a target="_blank" href="https://api.whatsapp.com/send?phone=<?= (int) filter_var(get_field('whatsapp', 'geral'), FILTER_SANITIZE_NUMBER_INT); ?>">
						<img src="<?= get_stylesheet_directory_uri() ?>/img/whatsapp.svg" alt="Ícone do Whatsapp">
					</a>
				</div>

				<div class="container-toggle-menu">
					<div class="toggle-menu debug-safari">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>

				<div class="container-menu-responsivo">
					<ul class="menu-responsivo">
						<li>
							<a class="fonte" href="/">Home</a>
						</li>
						<li class="pai-link-produtos">
							<a class="fonte link-produtos" href="/produtos">
								Produtos
								<img src="<?= get_stylesheet_directory_uri() ?>/img/arrow_down.svg" class="seta-dropdown">
							</a>
							<?php
								$produtos = new produtos();
								echo $produtos->dropdown_header_responsivo();
							?>
						</li>
						<div class="header-logo">
							<a href="/">
								<img src="<?= get_stylesheet_directory_uri() ?>/img/logo.svg">
							</a>
						</div>
						<li>
							<a class="fonte" href="/#secao-apos-video">Sobre Nós</a>
						</li>
						<li>
							<a class="fonte" onclick="abrirModalContato()">Contato</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>
</div>
	<script>

	function ajustarTopoMenuResponsivo(){
		let menuResponsivo = $('.header .filho-container .container-menu-responsivo .menu-responsivo')[0];

		menuResponsivo.style.top = '-'+ menuResponsivo.offsetHeight +'px';
	}
	jQuery(window).resize(function($){
		ajustarTopoMenuResponsivo();
	});

	ajustarTopoMenuResponsivo();

	function abrirModalContato(){
		var modalContato = document.querySelectorAll('.modal-contato')[0];
		var fundoEscuro = document.querySelectorAll('.fundo-escuro')[0];
		var classeAbrir = 'active';

		adicionarClasse(modalContato, classeAbrir);
		adicionarClasse(fundoEscuro, classeAbrir);
	}

	</script>
	<section class="secao-conteudo">