<style>
/**************************************************************************************
********************************* VIDEO
**************************************************************************************/
.flt_video{
	display: none;
	position: fixed;
	width: 100%;
	height: 100vh;
	top: 0;
	left: 0;
	background-color: rgba(0, 0, 0, 0.6);
	z-index: 150;
	justify-content: center;
	align-items: center;
	overflow: auto;
}

.bg_flt_vid{
	float: left;
	width: 100%;
	height: 100%;
}

.fechar_flt_vid{
	position: absolute;
	top: 5px;
	right: 5px;
	text-transform: uppercase;
	font-size: 12px;
	color: #ffffff;
	font-weight: 700;
	letter-spacing: 1px;
	cursor: pointer;
	z-index: 100;
	padding: 6px;
	background-color: #000000;
}

.fechar_flt_vid:hover{
	color: #240f20;	
}

.blc_flt_vid{
	position: absolute;
	max-width: 1440px;
	width: 100%;
}
</style>

<section class="flt_video">
	<div class="bg_flt_vid" onclick="fecha_video();"></div>
	<div class="fechar_flt_vid tra" onclick="fecha_video();">X fechar</div>	
	<div class="blc_flt_vid">
		<div class="iframe_video"></div>
	</div>
</section>

<script>
	function abre_video(id, tipo){
		var iframe;
		if (tipo == 'youtube') {
			iframe = '<iframe width="100%" height="900" src="https://www.youtube.com/embed/'+ id + '?autoplay=1" frameborder="0" allowfullscreen></iframe>';	
		}else if(tipo == "vimeo"){
			iframe = '<iframe src="https://player.vimeo.com/video/'+ id +'?autoplay=1" width="100%" height="900" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
		}

		jQuery('html').css("overflow", "hidden");
		jQuery('.iframe_video').html(iframe);
		jQuery('.flt_video').css("display", "flex").hide().fadeIn(400);		
	}

	function fecha_video(){
		jQuery('.flt_video').fadeOut(200);
		jQuery('.iframe_video').html('');
		jQuery('html').css("overflow", "scroll");	
	}

	jQuery(document).keydown(function(e) {
	    if (e.keyCode == 27) {
	    	fecha_video();
	    }
	});
</script>