jQuery(document).ready(function ($) {
    var $form = $('form[data-hc-form]');

    if ( ! $form.length) {
        console.log('no form[data-hc-form] found');
        return;
    }

    $form.each(function () {
        var $_form = $(this);

        var $button = $_form.find('button[type=submit]');

        if ( ! $button.length) {
            console.log('no button[type=submit] found');
            return;
        }
    
        var oldText = $button.text();

        $_form.ajaxForm({
            dataType: 'json',
            data: {
                action: hc.action
            },
            beforeSend: function () {
                $button.text(hc.loadingText).attr('disabled', '');
            },
            complete: function () {
                $button.text(oldText).removeAttr('disabled');
            },
            success: function (response) {
                console.log(response);
                $form.trigger('hibridoContactAjaxSuccess', [response]);

                if (response.success) {
                    toastr.success(response.message);
                    this.reset();
                } else {
                    toastr.error(response.message);
                }
                
            },
            error: function( err){
                console.log(err);
            }
        });
    });
});